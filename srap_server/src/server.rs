/* SPDX-License-Identifier: AGPL-3.0-or-later
Rust reference implementation of Stream Resource Application Protocol.
Copyright (C) 2021 Alex Martin

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

//! Module providing support for accepting connections.

use std::{net::SocketAddr, fs, path::PathBuf, marker::PhantomData};
use futures::StreamExt;
use quinn::{ServerConfig, Endpoint, CertificateChain, PrivateKey, Incoming, NewConnection};
use rcgen::generate_simple_self_signed;

use crate::resource::Resource;
use crate::session::Session;
use crate::connection::Connection;

/// An SRAP server accepting client connections.
pub struct Server<S: Session<R>, R: Resource> {
    /// New client connections.
    listener: Incoming,
    /// We need to have `S` as a type parameter for [`Connection`], but we don't need it on any fields.
    phantom_session: PhantomData<S>,
    /// `S`'s [`Session<R>`] needs `R`, and we need to have it as a type parameter for [`Connection`], but we don't need it on any fields.
    phantom_resource: PhantomData<R>,
}

/// An error in initializing a [`Server`].
#[derive(Debug)]
pub enum SetupError {
    /// Binding the socket to an address failed.
    BindFailed(quinn::EndpointError),
    /// Validating and configuring the TLS certificate failed.
    CertSetupFailed(rustls::TLSError),
    /// Parsing the TLS certificate failed.
    CertParseFailed(quinn::ParseError),
    /// An IO operation related to certificate storage failed.
    CertIOFailed(std::io::Error),
    /// Generating a self-signed certificate failed.
    CertGenerateFailed(rcgen::RcgenError),
}

impl std::fmt::Display for SetupError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> Result<(), std::fmt::Error> {
        use SetupError::*;
        match self {
            BindFailed(err) => write!(f, "Binding the socket to an address failed: {}", err),
            CertSetupFailed(err) => write!(f, "Validating and configuring the TLS certificate failed: {}", err),
            CertParseFailed(err) => write!(f, "Parsing the TLS certificate failed: {}", err),
            CertIOFailed(err) => write!(f, "An IO operation related to certificate storage failed: {}", err),
            CertGenerateFailed(err) => write!(f, "Generating a self-signed certificate failed: {}", err),
        }
    }
}

impl std::error::Error for SetupError {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        use SetupError::*;
        Some(match self {
            BindFailed(err) => err,
            CertSetupFailed(err) => err,
            CertParseFailed(err) => err,
            CertIOFailed(err) => err,
            CertGenerateFailed(err) => err,
        })
    }
}

impl<S: Session<R>, R: Resource> Server<S, R> {
    /// Create a new [`Server`] to listen on the given [`SocketAddr`] using the given [`ServerConfig`].
    pub fn with_server_config(addr: &SocketAddr, config: ServerConfig) -> Result<Self, SetupError> {
        let mut builder = Endpoint::builder();
        builder.listen(config);
        let (_, incoming) = match builder.bind(addr) {
            Ok(ep) => ep,
            Err(err) => return Err(SetupError::BindFailed(err)),
        };

        Ok(Server{listener: incoming, phantom_session: PhantomData, phantom_resource: PhantomData})
    }

    /// Create a new [`Server`] to listen on the given [`SocketAddr`] using the given TLS certificate and private key, and all other configuration as default.
    pub fn with_cert(addr: &SocketAddr, chain: CertificateChain, key: PrivateKey) -> Result<Self, SetupError> {
        // quinn server config setup boilerplate.
        let mut transport = quinn::TransportConfig::default();
        transport.max_concurrent_uni_streams(0).unwrap(); // The client is not allowed to open unidirectional streams. Unwrap is safe because 0 is not out of bounds, which is the only error from max_concurrent_uni_streams().
        let mut config = ServerConfig::default();
        config.transport = std::sync::Arc::new(transport);

        // Set up cert.
        match config.certificate(chain, key) {
            Ok(_) => {},
            Err(err) => return Err(SetupError::CertSetupFailed(err)),
        };

        Server::with_server_config(addr, config)
    }

    /// Create a new [`Server`] to listen on the given [`SocketAddr`] using the TLS certificate and private key in the specified PEM-encoded files, and all other configuration as default.
    pub fn with_pem_files(addr: &SocketAddr, cert_path: &PathBuf, key_path: &PathBuf) -> Result<Self, SetupError> {
        let cert = match CertificateChain::from_pem(&(match fs::read(cert_path) {
            Ok(data) => data,
            Err(err) => return Err(SetupError::CertIOFailed(err)),
        })) {
            Ok(cert) => cert,
            Err(err) => return Err(SetupError::CertParseFailed(err)),
        };
        let key = match PrivateKey::from_pem(&(match fs::read(key_path) {
            Ok(data) => data,
            Err(err) => return Err(SetupError::CertIOFailed(err)),
        })) {
            Ok(key) => key,
            Err(err) => return Err(SetupError::CertParseFailed(err)),
        };
        Server::with_cert(addr, cert, key)
    }

    /// Create a new [`Server`] to listen on the given [`SocketAddr`] using the TLS certificate and private key in the specified DER-encoded files, and all other configuration as default.
    ///
    /// Only a single certificate is supported, not a full chain. Use a PEM-encoded certificate chain if you need support for non-self-signed certificates (which you should support!).
    pub fn with_der_files(addr: &SocketAddr, cert_path: &PathBuf, key_path: &PathBuf) -> Result<Self, SetupError> {
        let cert = CertificateChain::from_certs([match quinn::Certificate::from_der(&(match fs::read(cert_path) {
            Ok(data) => data,
            Err(err) => return Err(SetupError::CertIOFailed(err)),
        })) {
            Ok(cert) => cert,
            Err(err) => return Err(SetupError::CertParseFailed(err)),
        }]);
        let key = match PrivateKey::from_der(&(match fs::read(key_path) {
            Ok(data) => data,
            Err(err) => return Err(SetupError::CertIOFailed(err)),
        })) {
            Ok(key) => key,
            Err(err) => return Err(SetupError::CertParseFailed(err)),
        };
        Server::with_cert(addr, cert, key)
    }

    /// Create a new [`Server`] to listen on the given [`SocketAddr`] using a self-signed TLS certificate stored with its private key in the given files, and all other configuration as default. Can be used for testing or for trust-on-first-use authentication, since the certificate remains the same from one run to the next, at least until it expires.
    ///
    /// If the files don't exist or don't contain a valid PEM certificate and key (including if expired), a new self-signed certificate will be generated automatically.
    pub fn with_self_signed(addr: &SocketAddr, cert_path: &PathBuf, key_path: &PathBuf) -> Result<Self, SetupError> {
        // First try using existing files.
        if let Ok(server) = Server::with_pem_files(addr, cert_path, key_path) {
            Ok(server)
        } else {
            // Generate new keypair.
            let pair = match generate_simple_self_signed([format!("{}", addr)]) {
                Ok(pair) => pair,
                Err(err) => return Err(SetupError::CertGenerateFailed(err)),
            };

            // Serialize keypair to PEM.
            let cert_data = match pair.serialize_pem() {
                Ok(data) => data,
                Err(err) => return Err(SetupError::CertGenerateFailed(err)),
            };
            let key_data = pair.serialize_private_key_pem();

            // Write keypair to files.
            match fs::write(cert_path, cert_data) {
                Ok(_) => {},
                Err(err) => return Err(SetupError::CertIOFailed(err)),
            };
            match fs::write(key_path, key_data) {
                Ok(_) => {},
                Err(err) => return Err(SetupError::CertIOFailed(err)),
            };

            // Use files.
            Server::with_pem_files(addr, cert_path, key_path)
        }
    }

    /// Accept the next client and start an async task to handle the connection; yet more tasks will be started to handle each open resource.
    pub async fn another_client(&mut self) {
        match self.listener.next().await {
            Some(c) => match c.await {
                Ok(NewConnection{connection, bi_streams, ..}) => {
                    let conn = Connection::<S, R>::new(connection, bi_streams);
                    tokio::spawn(async move {conn.handle()});
                },
                Err(_) => {},
            },
            None => {},
        }
    }

    /// Convenience function to run [`another_client()`](Server::another_client) in an infinite loop. Good-quality servers should monitor their resource usage and stop accepting clients before they overload the machine.
    pub async fn infinite_clients(&mut self) {
        loop {
            self.another_client().await;
        }
    }
}

#[cfg(test)]
mod tests {
    // TODO: Write some tests.
}
