/* SPDX-License-Identifier: AGPL-3.0-or-later
Rust reference implementation of Stream Resource Application Protocol.
Copyright (C) 2021 Alex Martin

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

//! Library for Stream Resource Application Protocol (SRAP) servers.
//!
//! SRAP is a protocol supporting application-layer protocols by allowing clients to maintain multiple simultaneous sessions, in which they can open multiple resources, identified by URI, and exchange data with them via three byte streams, input, output, and feedback, similar to the Unix standard file descriptors. SRAP runs over QUIC.
//!
//! This reference implementation is based on the [`quinn`] library for QUIC support and [`tokio`] for asynchronous concurrency.
//!
//! A [`Server`] allows you to accept connections, and must be bound to a [`SocketAddr`] and TLS certificate at creation time. You can accept connections one at a time according to your available resources (recommended), or allow the server to accept an unlimited amount of connections.
//!
//! Connections themselves are transparent. When a client begins a new session, a [`Session`] object of a type you provide will be created. Each time the client opens a resource, this object's [`Session::open()`] method will be called, giving the [`URI`] of the resource and other relevant information.
//!
//! [`Session::open()`] can return an error, e.g. if the client requests a resource you've never heard of, or which requires authentication it hasn't provided, or return a [`Resource`], whose [`Resource::handle()`] method will be called with the streams connected to the client. Each session can have only one [`Resource`] type; if you have multiple kinds of resource, the easiest solution is to use an `enum` and call a different function for each variant.
//!
//! You can store information about each client that spans across multiple resources in the [`Session`], such as its user account, or temporary data like a guest shopping cart, though it will be lost if the client disconnects.
//!
//! Shutting down the [`tokio::runtime::Runtime`] from which you called [`Server::another_client()`] or [`Server::infinite_clients()`] will abruptly but gracefully drop active connections accepted by those methods, interrupting any sessions in progress with an uninformative connection-shut-down error.

extern crate quinn;
extern crate rustls;
extern crate rcgen;
extern crate futures;
extern crate tokio;
extern crate uriparse;

pub use uriparse::{URI, URIError};

mod resource;
pub use resource::*;

mod session;
pub use session::*;

mod connection;

mod server;
pub use server::*;

#[cfg(test)]
mod tests {
    // TODO: Write some tests.
}
