/* SPDX-License-Identifier: AGPL-3.0-or-later
Rust reference implementation of Stream Resource Application Protocol.
Copyright (C) 2021 Alex Martin

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

//! Module providing internal machinery for responding to newly-opened streams.

use std::{marker::PhantomData, collections::HashMap};
use futures::StreamExt;
use quinn::{IncomingBiStreams, RecvStream};
use uriparse::URI;
use crate::resource::Resource;
use crate::session::{Session, OpenError};

/// An active connection from a client, with possibly many sessions.
pub struct Connection<S: Session<R>, R: Resource> {
    /// The underlying QUIC connection itself.
    conn: quinn::Connection,
    /// New bidirectional streams from the client; only bidirectional streams may be used for resource opens. The client is not allowed to open unidirectional streams.
    streams: IncomingBiStreams,
    /// The mapping of sessions IDs to session handlers.
    sessions: HashMap<[u8; 8], S>,
    /// `S`'s [`Session<R>`] needs `R`, and we need to have it as a type parameter for [`Connection::handle`] so it can create the right kind of [`Resource`], but we don't need it on any fields.
    phantom_resource: PhantomData<R>,
}

/// Parse a query string for mode and options.
fn parse_query(query: Option<&uriparse::Query<'static>>) -> (Option<String>, HashMap<String, String>) {
    let mut config = HashMap::new();
    if let Some(query) = query {
        let mut split = query.as_str().split(":");
        let mode = match split.next() {
            Some(mode) => Some(mode.to_owned()),
            None => None,
        };
        for option in split {
            let mut option = option.split("=");
            let name = option.next();
            let value = option.next();
            if let Some(name) = name {
                if let Some(value) = value {
                    config.insert(name.to_owned(), value.to_owned());
                }
            }
        }
        (mode, config)
    } else {
        (None, config)
    }
}

impl<S: Session<R>, R: Resource> Connection<S, R> {
    pub fn new(conn: quinn::Connection, streams: IncomingBiStreams) -> Connection<S, R> {
        Connection{conn: conn, streams: streams, sessions: HashMap::new(), phantom_resource: PhantomData}
    }

    /// Read the [`URI`] from the client.
    async fn read_uri(in_stream: &mut RecvStream) -> Result<URI<'static>, OpenError> {
        let mut buf = Vec::<u8>::with_capacity(1024); // URIs can be 1024 bytes long.
        let mut latest = [(0 as u8)];
        while let Ok(Some(1)) = in_stream.read(&mut latest).await { // Read one byte at a time until newline is found.
            if latest[0] == b'\n' {
                break;
            }
            buf.push(latest[0]);
        }

        match URI::try_from(&buf[..]) {
            Ok(uri) => Ok(uri.into_owned()),
            Err(e) => Err(OpenError::CorruptUri{supposed_uri: "/".to_string(), err: e}),
        }
    }

    /// Handle dispatching resource opens to [`Session::open()`], creating new [`Session`]s as appropriate.
    pub async fn handle(mut self) {
        loop {
            // Get new bidirectional stream from client.
            let (mut feedback_stream, mut in_stream) = match self.streams.next().await {
                Some(result) => match result {
                    Ok(streams) => streams,
                    Err(_) => break, // Transport is gone, jump off the cliff and take the whole Connection with us.
                },
                None => continue,
            };

            // Read session ID.
            let mut session_id = [0 as u8; 8];
            match in_stream.read_exact(&mut session_id).await {
                Ok(_) => {},
                Err(_) => {
                    let _ = feedback_stream.write(b"ECorrupt open\n").await; // E = Reason-only error.
                    continue;
                },
            };

            // Get or create session.
            let session = match self.sessions.get_mut(&session_id) {
                Some(existing_session) => existing_session,
                None => {
                    let new_session = S::new();
                    self.sessions.insert(session_id, new_session);
                    self.sessions.get_mut(&session_id).unwrap()
                },
            };

            // Read/parse the URI.
            let uri = match Self::read_uri(&mut in_stream).await {
                Ok(uri) => uri,
                Err(e) => {
                    feedback_stream.write(&e.to_wire_format());
                    continue;
                },
            };
            let (mode, config) = if uri.scheme().as_str().to_lowercase() == "srap" { // to_lowercase() instead of normalize() to avoid mutating URI.
                parse_query(uri.query())
            } else {
                (None, HashMap::new())
            };

            // Open the requested resource. Hang onto the URI for the last error opportunity.
            let resource = match session.open(uri.clone(), mode, config) {
                Ok(resource) => resource,
                Err(e) => {
                    feedback_stream.write(&e.to_wire_format());
                    continue;
                },
            };

            // Open the output stream; this is the last opportunity for an open error.
            let out_stream = match self.conn.open_uni().await {
                Ok(s) => s,
                Err(_) => {
                    let _ = feedback_stream.write(&OpenError::MessageOnly{uri: uri, reason: "Internal server error".to_string()}.to_wire_format()).await;
                    continue;
                },
            };

            // Signal a successful open and send the output stream ID.
            let _ = feedback_stream.write(&[b'S']).await; // S = Success
            let _ = feedback_stream.write(&Into::<quinn::VarInt>::into(out_stream.id()).into_inner().to_be_bytes()).await; // FIXME: Fixed size; should QUIC varints be used on the wire?

            // Let the resource take over from here.
            tokio::spawn(async move {resource.handle(in_stream, out_stream, feedback_stream)});
        }
    }
}

#[cfg(test)]
mod tests {
    // TODO: Write some tests.
}
