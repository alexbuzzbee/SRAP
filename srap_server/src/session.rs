/* SPDX-License-Identifier: AGPL-3.0-or-later
Rust reference implementation of Stream Resource Application Protocol.
Copyright (C) 2021 Alex Martin

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

//! Module providing trait for session handlers.

use std::collections::HashMap;
use uriparse::{URI, URIError};

use crate::resource::Resource;

/// An error in opening a resource to be returned to the client.
#[derive(Debug)]
pub enum OpenError {
    /// The URI was corrupt. The supposed URI is provided along with the underlying error.
    CorruptUri {
        /// The string that was passed as a URI.
        supposed_uri: String,
        /// The error that occurred parsing the URI.
        err: URIError
    },
    /// An error that gives another resource (locally or remotely) the client should access instead to accomplish what it intended, along with the requested URI and a reason string.
    Redirect {
        /// The URI the client tried to open.
        uri: URI<'static>,
        /// The reason for the redirect.
        reason: String,
        /// The destination of the redirect.
        target: URI<'static>
    },
    /// An error that gives another resource from which the client can read more information about the error, along with the requested URI and a reason string.
    WithInfo {
        /// The URI the client tried to open.
        uri: URI<'static>,
        /// A short summary of the reason for the error.
        reason: String,
        /// A URI pointing to a resource from which the client can read (if an SRAP URI, by opening and immediately reading to the end of the output stream, with no input needed and no specific meaning to the feedback) more information about the error in some format. Any relevant mode and options should be included so smart clients can understand what they're reading.
        info: URI<'static>
    },
    /// An error that gives only the requested URI and a reason string.
    MessageOnly {
        /// The URI the client tried to open.
        uri: URI<'static>,
        /// The reason for the error.
        reason: String
    },
}

impl OpenError {
    /// Get the [`URI`] the client was trying to open. If it was invalid, the string that wasn't a URI and the error encountered parsing it are returned.
    pub fn opened_uri(&self) -> Result<&URI, (&str, &URIError)> {
        use OpenError::*;
        match self {
            CorruptUri{supposed_uri, err} => Err((supposed_uri, err)),
            Redirect{uri, ..} => Ok(uri),
            WithInfo{uri, ..} => Ok(uri),
            MessageOnly{uri, ..} => Ok(uri),
        }
    }

    /// Get the URI the client was trying to open, as a string, even if it isn't a valid URI.
    pub fn opened_uri_str(&self) -> String {
        match self.opened_uri() {
            Ok(uri) => uri.to_string(),
            Err((supposed_uri, _)) => supposed_uri.to_owned(),
        }
    }

    /// Get the reason for the error.
    pub fn reason(&self) -> String {
        use OpenError::*;
        match self {
            CorruptUri{err, ..} => format!("Corrupt URI: {}", err),
            Redirect{reason, ..} => reason.to_owned(),
            WithInfo{reason, ..} => reason.to_owned(),
            MessageOnly{reason, ..} => reason.to_owned(),
        }
    }

    /// Get the [`URI`] the client should try again at.
    pub fn redirect_uri(&self) -> Option<&URI> {
        use OpenError::*;
        match self {
            Redirect{target, ..} => Some(target),
            _ => None,
        }
    }

    /// Get the [`URI`] the client should get info from.
    pub fn info_uri(&self) -> Option<&URI> {
        use OpenError::*;
        match self {
            WithInfo{info, ..} => Some(info),
            _ => None,
        }
    }

    /// Get the SRAP on-the-wire error ID byte for this error.
    pub fn error_id(&self) -> u8 {
        use OpenError::*;
        match self {
            CorruptUri{..} => b'E',
            MessageOnly{..} => b'E',
            Redirect{..} => b'R',
            WithInfo{..} => b'I',
        }
    }

    /// Get the error in the SRAP on-the-wire format: The error ID, the reason, a newline, and either the redirect or info URI and its own newline if present.
    pub fn to_wire_format(&self) -> Vec<u8> {
        let mut vec = Vec::new();
        vec.push(self.error_id());
        vec.extend(self.reason().into_bytes());
        vec.push(b'\n');
        match self.redirect_uri() {
            Some(u) => {
                vec.extend(u.to_string().into_bytes());
                vec.push(b'\n');
            },
            None => {}
        }
        match self.info_uri() {
            Some(u) => {
                vec.extend(u.to_string().into_bytes());
                vec.push(b'\n');
            },
            None => {}
        }
        vec
    }
}

impl std::fmt::Display for OpenError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> Result<(), std::fmt::Error> {
        write!(f, "Error opening '{}': {}", self.opened_uri_str(), self.reason());
        match self.redirect_uri() {
            Some(uri) => {write!(f, "; Redirect to {}", uri);},
            None => {},
        };
        match self.info_uri() {
            Some(uri) => {write!(f, "; More information at {}", uri);},
            None => {},
        };
        Ok(())
    }
}

impl std::error::Error for OpenError {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        use OpenError::*;
        match self {
            CorruptUri{err, ..} => Some(err),
            _ => None
        }
    }
}

/// An active session from a client. Dropped when the client disconnects. [`Session`]s do not correspond 1:1 with connections; there can be multiple sessions per connection. However, all sessions should be treated as unrelated.
pub trait Session<R: Resource>: Send + 'static {
    /// Create a [`Session`].
    fn new() -> Self;
    /// Open the [`Resource`] at the given URI. The mode specifies what interface the client is requesting, and the options provide additional mode-specific configuration. They appear only when the URI scheme is `srap` (case insensitive), and are parsed from the query string, which is also included in the URI.
    fn open(&mut self, uri: URI, mode: Option<String>, options: HashMap<String, String>) -> Result<R, OpenError>;
}

#[cfg(test)]
mod tests {
    // TODO: Write some tests.
}
